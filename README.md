### PHEtools

A collection of functions and tools to facilitate public health data analysis.  Commonly wrapping around existing R functions, they expose a more explicit interface to the functions allowing quick and easy analysis of data.

### Installation

Currently the software is held in a Git repository at Bitbucket.org [PHEtools](https://bitbucket.org/spadehed/phetools), and installation can be done either by downloading a current release (if available) or by using devtools as below.


```R
install.packages('devtools')
devtools:::install_bitbucket("spadehed/phetools")
```

The latter will download the current development version of the package, and may not be 100% stable.